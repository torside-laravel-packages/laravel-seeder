<?php

namespace Torside\LaravelSeeder\Commands;

use Illuminate\Console\Command;

class LaravelSeeder extends Command
{
    const DELIMETER = ',';

    /** @var string $signature */
    protected $signature = 'torside:seeder';

    /** @var string $description */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        /** @var array $tables */
        $tables = array_filter(array_map(function (string $model) {
            if (class_exists($model) && defined("{$model}::TABLE_NAME")) {
                return constant("{$model}::TABLE_NAME");
            }
        }, config('torside.laravel-seeder.settings.models', [])));

        $this->call('iseed', [
            'tables' => implode(self::DELIMETER, $tables),
            '--force'
        ]);
    }
}