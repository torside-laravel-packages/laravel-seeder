<?php

namespace Torside\LaravelSeeder\Providers;

use Illuminate\Support\ServiceProvider;
use Torside\LaravelSeeder\Commands\LaravelSeeder;

class LaravelSeederServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__ . '/../Config/settings.php' => config_path('torside/laravel-seeder/settings.php'),
        ]);

        if ($this->app->runningInConsole()) {
            $this->commands([
                LaravelSeeder::class
            ]);
        }
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
    }
}